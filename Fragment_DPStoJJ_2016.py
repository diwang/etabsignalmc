import FWCore.ParameterSet.Config as cms
from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.MCTunes2017.PythiaCP5Settings_cfi import *

generator = cms.EDFilter("Pythia8GeneratorFilter",
   crossSection = cms.untracked.double(1.0),
   maxEventsToPrint = cms.untracked.int32(0),
   pythiaPylistVerbosity = cms.untracked.int32(1),
   filterEfficiency = cms.untracked.double(0.3),
   pythiaHepMCVerbosity = cms.untracked.bool(False),
   comEnergy = cms.double(13000.0),
   PythiaParameters = cms.PSet(
      pythia8CommonSettingsBlock, 
      pythia8CP5SettingsBlock,
      processParameters = cms.vstring( 
           'Charmonium:gg2ccbar(3S1)[3S1(1)]g = on, off',
#'Charmonium:gg2ccbar(3S1)[3S1(1)]gm = on, off',
           'Charmonium:gg2ccbar(3S1)[3S1(8)]g = on, off',
           'Charmonium:qg2ccbar(3S1)[3S1(8)]q = on, off',
           'Charmonium:qqbar2ccbar(3S1)[3S1(8)]g = on, off',
           'Charmonium:gg2ccbar(3S1)[1S0(8)]g = on, off',
           'Charmonium:qg2ccbar(3S1)[1S0(8)]q = on, off',
           'Charmonium:qqbar2ccbar(3S1)[1S0(8)]g = on, off',
           'Charmonium:gg2ccbar(3S1)[3PJ(8)]g = on, off',
           'Charmonium:qg2ccbar(3S1)[3PJ(8)]q = on, off',
           'Charmonium:qqbar2ccbar(3S1)[3PJ(8)]g = on, off',
           'PartonLevel:MPI = on',
           'SecondHard:generate = on',
           'SecondHard:Charmonium = on',
           'Charmonium:all =off',
           '443:onMode = off',
           '443:onIfMatch = -13 13',
      ),
      parameterSets = cms.vstring(
           'pythia8CommonSettings',
           'pythia8CP5Settings',
           'processParameters',
      )
   )
)

FourMuonFilter = cms.EDFilter("FourLepFilter", 
        MinPt = cms.untracked.double(1), # change from 1.9 to 1
        MaxPt = cms.untracked.double(4000.0),
        MaxEta = cms.untracked.double(2.5),
        MinEta = cms.untracked.double(0.0),
        ParticleID = cms.untracked.int32(13)
)

ProductionFilterSequence = cms.Sequence(generator*FourMuonFilter)
