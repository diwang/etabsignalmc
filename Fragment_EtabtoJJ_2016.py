import FWCore.ParameterSet.Config as cms
from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.Pythia8CUEP8M1Settings_cfi import *

generator = cms.EDFilter("Pythia8GeneratorFilter",
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    maxEventsToPrint = cms.untracked.int32(0),
    pythiaPylistVerbosity = cms.untracked.int32(0),
    filterEfficiency = cms.untracked.double(1.0),
    crossSection = cms.untracked.double(1.0),
    comEnergy = cms.double(13000.0),
    PythiaParameters = cms.PSet(pythia8CommonSettingsBlock,
        pythia8CUEP8M1SettingsBlock,
        processParameters = cms.vstring(
            'Higgs:useBSM = on',
            'HiggsBSM:gg2H2 = on',
            'HiggsH2:coup2d = 10.0',
            'HiggsH2:coup2u = 10.0',
            'HiggsH2:coup2Z = 0.0',
            'HiggsH2:coup2W = 0.0',
            'HiggsA3:coup2H2Z = 0.0',
            'HiggsH2:coup2A3A3 = 0.0',
            'HiggsH2:coup2H1H1 = 0.0',
            '443:onMode = off',
            '443:onIfMatch 13 13',
            '333:onMode = off',
            '333:onIfMatch 13 13',
            '553:onMode = off',
            '553:onIfMatch 13 13',
            '35:mMin = 0',
            '35:mMax = 50.0',
            '35:m0 = 9.4',
            '35:mWidth = 0.01',
            '35:addChannel 1 1.00 100 443 443',
            '35:onMode = off',
            '35:onIfMatch 443 443'
        ),
        parameterSets = cms.vstring('pythia8CommonSettings',
                                    'pythia8CUEP8M1Settings',
                                    'processParameters',
            )
    )
)

FourMuonFilter = cms.EDFilter("FourLepFilter", # require 4-mu in the final state
        MinPt = cms.untracked.double(1.8),
        MaxPt = cms.untracked.double(4000.0),
        MaxEta = cms.untracked.double(2.5),
        MinEta = cms.untracked.double(0.),
        ParticleID = cms.untracked.int32(13)
)

DiJpsiFilter = cms.EDFilter("MCParticlePairFilter",  # require 2-mu mass to be 8.5 - 11.5 GeV
        MinPt = cms.untracked.vdouble(1.0,1.0),
        MaxPt = cms.untracked.vdouble(4000.0,4000.0),
        MaxEta = cms.untracked.vdouble( 2.5,2.5),
        MinEta = cms.untracked.vdouble(-2.5,-2.5),
        ParticleID1 = cms.untracked.vint32(443, -443),
        ParticleID2 = cms.untracked.vint32(443, -443),
        MinInvMass = cms.untracked.double(1.0),
        MaxInvMass = cms.untracked.double(16.0),
)

ProductionFilterSequence = cms.Sequence(generator*FourMuonFilter*DiJpsiFilter)
