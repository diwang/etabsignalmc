# EtabSignalMC

Fragment_EtabtoJJ_2016.py Fragment_EtabtoJJ_2017.py Fragment_EtabtoJJ_2018.py are private MC generation scripts of Etab signal  

The Etab particle will decay into double JPsi, then the finial state is 4 muons  

We need UL MC samples for 2016, 2017 and 2018. The tool we use is Pythia8  

This script should be used under CMSSW_10_6_20_patch1  

The Filter efficiency (event-level) is 1.268e-01  

For each year we require 2e5 events(before filter)  

We also need to go though rest steps, for example DIGIRAW, HLT and RECO  

# SPS Background MC

Fragment_SPStoJJ_Direct_2016.py Fragment_SPStoJJ_Direct_2017.py Fragment_SPStoJJ_Direct_2018.py are private MC generation of SPS background  

We need UL MC samples of SPS background for 2016, 2017 and 2018  

This script should be used under CMSSW_10_6_20_patch1  

The Filter efficiency (event-level) is 1e-4  

For each year we require 2e6 events(before filter)  

# DPS Background MC

Fragment_DPStoJJ_2016.py Fragment_DPStoJJ_2017.py Fragment_DPStoJJ_2018.py are private MC generation of DPS background  

We need UL MC samples of DPS background for 2016, 2017 and 2018  

This script should be used under CMSSW_10_6_20_patch1  

The Filter efficiency (event-level) is 6e-5  

For each year we require 10e12 events(before filter)

